module.exports = function(app) {
  if(typeof app.channel !== 'function') {
    // If no real-time functionality has been configured just return
    return;
  }

  app.on('connection', connection => {
    // On a new real-time connection, add it to the anonymous channel
    console.log(connection)
    console.log(`has joined the anonymous room`)
    app.channel('anonymous').join(connection);
  });

  app.on('login', (authResult, { connection }) => {
    // connection can be undefined if there is no
    // real-time connection, e.g. when logging in via REST
    if(connection) {
      // Obtain the logged in user from the connection
      // const user = connection.user;

      // The connection is no longer anonymous, remove it
      app.channel('anonymous').leave(connection);
      console.log(`${connection.user.username} is entering the authenticated zone`)

      // Add it to the authenticated user channel
      app.channel('authenticated').join(connection);

      // Channels can be named anything and joined on any condition

      // E.g. to send real-time events only to admins use
      // if(user.isAdmin) { app.channel('admins').join(connection); }

      // If the user has joined e.g. chat rooms
      // if(Array.isArray(user.rooms)) user.rooms.forEach(room => app.channel(`rooms/${room.id}`).join(channel));

      // Easily organize users by email and userid for things like messaging
      // app.channel(`emails/${user.email}`).join(channel);

      app.channel(`userIds/${connection.payload.userId}`).join(connection);
      //subscribe to followers actions
      //subscribe to most recent ideas

    }
  });

  // eslint-disable-next-line no-unused-vars
  // app.publish((data, hook) => {
  //   // Here you can add event publishers to channels set up in `channels.js`
  //   // To publish only for a specific evenhttps://docs.feathersjs.com/api/channels.htmlt use `app.publish(eventname, () => {})`
  //
  //   console.log('Publishing all events to all authenticated users. See `channels.js` and https://docs.feathersjs.com/api/channels.html for more information.'); // eslint-disable-line
  //   console.log("channel data")
  //   console.log(data)
  //
  //   // e.g. to publish all service events to all authenticated users use
  //   return app.channel('authenticated');
  // });

  app.service('ideas').publish((data, hook) => {
    return [
      app.channel(`authenticated`),
      app.channel(`ideaIds/${hook.data._id}`),
      //app.channel(`emails/${data.recipientEmail}`)
    ];
  });
  //notify Idea author that an upvote was made
  app.service('votes').publish(async (data, hook) => {
    console.log("fething author of " + data.idea_id )
    const idea = await app.service('ideas').get(data.idea_id);

    console.log("publishing to author "  + idea.user_id)
    return [
      app.channel(`userIds/${idea.user_id}`),
      //app.channel(`emails/${data.recipientEmail}`)
    ];
  });
  //notify Idea author that a comment was made
  app.service('comments').publish(async (data, hook) => {
    console.log("fething author of original idea " + hook.data.idea_id )

    console.log(Object.keys(hook.data))
    const idea = await app.service('ideas').get(hook.data.idea_id);
    console.log("publishing comment to author "  + idea.user_id)
    return [
      app.channel(`userIds/${idea.user_id}`),
      //app.channel(`emails/${data.recipientEmail}`)
    ];
  });

  app.service('followers').publish(async (data, hook) => {
    console.log("posting followers events")

    return [
      app.channel(`userIds/${hook.data.user_id}`),
      app.channel(`userIds/${hook.data.following_id}`)
    ];
  });



  // Here you can also add service specific event publishers
  // e.g. the publish the `users` service `created` event to the `admins` channel
  // app.service('users').publish('created', () => app.channel('admins'));

  // With the userid and email organization from above you can easily select involved users
  // app.service('votes').publish(() => {
  //   return [
  //     app.channel(`userIds/${data.user_id}`),
  //     //app.channel(`emails/${data.recipientEmail}`)
  //   ];
  // });
  // app.service('ideas').publish(() => {
  //   return [
  //     app.channel(`userIds/${data.user_id}`),
  //     //app.channel(`emails/${data.recipientEmail}`)
  //   ];
  // });
};
