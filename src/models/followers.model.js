// followers-model.js - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const followers = new Schema({
    user_id: { type: String, required: true },
    follower_id: { type: String, required: true }
  }, {
    timestamps: true
  });

  followers.index({ user_id:1, follower_id:1 }, {"unique": true});


  // This is necessary to avoid model compilation errors in watch mode
  // see https://github.com/Automattic/mongoose/issues/1251
  try {
    return mongooseClient.model('followers');
  } catch (e) {
    return mongooseClient.model('followers', followers);
  }
};
