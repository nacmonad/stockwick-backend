// mongoose-example-model.js - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const votesSchema = new Schema({
    idea_id: { type: String, required: true },
    user_id: { type: String, required: true },
    username: {type: String, required: false }
  }, {
    timestamps: true
  });

  //enforce unique compound key
  votesSchema.index({ idea_id:1, user_id:1 }, {"unique": true});

  return mongooseClient.model('votes', votesSchema);
};
