// comments-model.js - A mongoose model
//
// See http://mongoosejs.com/docs/models.html
// for more of what you can do here.
module.exports = function (app) {
  const mongooseClient = app.get('mongooseClient');
  const { Schema } = mongooseClient;
  const comments = new Schema({
    idea_id: {type: String, required: true},
    author_id:{type: String, required: true},
    author_avatar:{type: String, required: false},
    text: { type: String, required: true }
  }, {
    timestamps: true
  });

  return mongooseClient.model('comments', comments);
};
