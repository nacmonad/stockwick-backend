module.exports = function (options = {}) { // eslint-disable-line no-unused-vars
  return async context => {
    const { app, data, params, result } = context;

    console.log("REMOVE COMMENTS ASSOCIATED WITH IDEA: " + result._id)
    console.log(Object.keys(result))

    try {
      await app.service('comments').remove(null, {idea_id:result._id})
    } catch (e) {
      console.log(e)
    } 
    console.log("STRAY COMMENTS REMOVED")
    // Best practice: hooks should always return the context
    return context;
  };
};
