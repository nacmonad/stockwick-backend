var ObjectId = require('mongodb').ObjectID

module.exports = function (options = {}) { // eslint-disable-line no-unused-vars
  return async context => {

    const { app, data } = context;


    console.log("PROCESSING UPTOKE:")

    if(!data.idea_id) {
      throw new Error('A vote must have an idea_id');
    }

    // The authenticated user
    const user = context.params.user;
    const theIdea = await app.service('ideas').get(data.idea_id);
    console.log('got idea ' + theIdea._id + ' with user' + theIdea.user_id + ' / ' + user._id)
    if(user._id.equals(theIdea.user_id) ) {
      throw new Error(`You can't upvote your own idea.`);
    }


    // Override the original data (so that people can't submit additional stuff)
    context.data = {
      idea_id: new ObjectId(data.idea_id),
      user_id: user._id,
      username: user.username,
      createdAt: new Date().getTime(),
    };

    // Best practice: hooks should always return the context
    return context;
  };
};
