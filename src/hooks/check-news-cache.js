//BEFORE NEWS HOOK TO FETCH
const app = require('../app');
const axios = require('axios');
const newsApiKey = `e3898b6ea8214f45b5cc3c9becb5de75`;

const queryString = require('query-string');
const NodeCache = require('node-cache');
const NewsCache = new NodeCache();


// eslint-disable-next-line no-unused-vars
module.exports = function (options = {}) {
  return async context => {
    let {app, params} = context;
    console.log("before news hook for query:")
    console.log(params.query)

    try {
      let qs = queryString.stringify(params.query)
      console.log("got qs: " + qs)
      let c = await NewsCache.get(qs);

      if( !c ){
          console.log("fresh request for " + qs)

          context.params.data = await axios.get(`https://newsapi.org/v2/top-headlines?${qs}&apiKey=${app.get('newsApiKey')}`).then(res=>res.data);
          await NewsCache.set(qs, context.params.data)
      } else {
        console.log("found cached param for query " + qs)
        context.params.data = {
          ...c
        };
      }

    } catch (e) {
        console.log(e)
    }

    return context;
  };
};
