module.exports = function (options = {}) { // eslint-disable-line no-unused-vars
  return async context => {
    const { data, params, result } = context;

    console.log("POPULATE THE IDEA WITH COMMENTS:")

    if(result.data.length > 0) {
      result.data = await Promise.all(result.data.map(async idea=>{
        console.log("fetching comments for idea " + idea._id)
        let comments = await context.app.service('comments').find({idea_id:idea._id});
        // console.log("comments: ")
        // console.log(comments)
        return {
          ...idea,
          comments:comments.data
        }
      }));
    }
    // Best practice: hooks should always return the context
    return context;
  };
};
