// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html

// eslint-disable-next-line no-unused-vars
module.exports = function (options = {}) {
    return async context => {
      let { data, params } = context;
      console.log("PROCESSING COMMENT:")
      console.log(Object.keys(data))
      // Throw an error if we didn't get a text
      if(!data.text) {
        throw new Error('A comment must have a text');
      }
      if(!data.idea_id) {
        throw new Error('A comment must be in reference to an idea thread.')
      }

      // The authenticated user
      const user = context.params.user;
      context.data = {
        ...data,
        author_id: user._id,
        author_avatar: user.img64
      }
    return context;
  };
};
