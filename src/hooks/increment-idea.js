var ObjectId = require('mongodb').ObjectID

module.exports = function (options = {}) { // eslint-disable-line no-unused-vars
  return async context => {
    const { app, data, params } = context;

    if(!data.idea_id) {
      throw new Error('A returned upvote must have an idea_id');
    }
    console.log("INCREMENTING UPTOKE ON IDEA:")
    console.log(data.idea_id)
    try {
        await app.service('ideas').update(data.idea_id,{$inc: {likes:1}}, {upsert:false})
        console.log("IDEA INCREMENTED " + data.idea_id)
    } catch(e) {
      console.log(e)
      context.data = {
        error:e.type,
        message:e.msg
      };
    }

    // // Override the original data (so that people can't submit additional stuff)
    // context.data = {
    //   idea_id: context.params.idea_id,
    //   user_id: user._id,
    //   username: user.username,
    //   createdAt: new Date().getTime(),
    // };

    // Best practice: hooks should always return the context
    return context;
  };
};
