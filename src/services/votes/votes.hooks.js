const { authenticate } = require('@feathersjs/authentication').hooks;
const {restrictToOwner, associateCurrentUser} = require('feathers-authentication-hooks');

const processVote = require('../../hooks/process-vote');
const incrementIdea = require('../../hooks/increment-idea');

module.exports = {
  before: {
    all: [ authenticate('jwt') ],
    find: [],
    get: [],
    create: [processVote()],
    update: [restrictToOwner({ idField: '_id', ownerField: 'user_id' })],
    patch: [restrictToOwner({ idField: '_id', ownerField: 'user_id' })],
    remove: [restrictToOwner({idField:'_id', ownerField: 'user_id'})]
  },
  after: {
    all: [],
    find: [],
    get: [],
    create: [incrementIdea()],
    update: [],
    patch: [],
    remove: []
  },
  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
