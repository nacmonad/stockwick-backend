const { authenticate } = require('@feathersjs/authentication').hooks;
const {restrictToOwner, associateCurrentUser} = require('feathers-authentication-hooks');

const processIdea = require('../../hooks/process-idea');
const populateIdeaWithComments = require('../../hooks/populate-idea-with-comments');
const removeCommentsAssociatedWithIdea = require('../../hooks/remove-comments-associated-with-idea');

module.exports = {
  before: {
    all: [ authenticate('jwt') ],
    find: [],
    get: [],
    create: [processIdea()],
    update: [restrictToOwner({ idField: '_id', ownerField: 'user_id' })],
    patch: [restrictToOwner({ idField: '_id', ownerField: 'user_id' })],
    remove: [restrictToOwner({ idField: '_id', ownerField: 'user_id' })]
  },

  after: {
    all: [],
    find: [populateIdeaWithComments()],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [removeCommentsAssociatedWithIdea()]
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
