// Initializes the `ideas` service on path `/ideas`
const createService = require('feathers-mongodb');
const hooks = require('./ideas.hooks');

module.exports = function (app) {
  const paginate = app.get('paginate');
  const mongoClient = app.get('mongoClient');
  const options = { paginate };

  // Initialize our service with any options it requires
  app.use('/ideas', createService(options));

  // Get our initialized service so that we can register hooks and filters
  const service = app.service('ideas');

  mongoClient.then(db => {
    service.Model = db.collection('ideas');
  });

  service.hooks(hooks);
};
