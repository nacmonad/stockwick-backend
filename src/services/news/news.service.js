// Initializes the `news` service on path `/news`
const createService = require('./news.class.js');
const hooks = require('./news.hooks');

module.exports = function (app) {
  
  const paginate = app.get('paginate');

  const options = {
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/news', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('news');

  service.hooks(hooks);
};
