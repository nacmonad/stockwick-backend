const { authenticate } = require('@feathersjs/authentication').hooks;

const checkNewsCache = require('../../hooks/check-news-cache');

module.exports = {
  before: {
    all: [ authenticate('jwt') ],
    find: [checkNewsCache()],
    get: [checkNewsCache()],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
