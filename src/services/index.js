const users = require('./users/users.service.js');
const messages = require('./messages/messages.service.js');
const ideas = require('./ideas/ideas.service.js');
const votes = require('./votes/votes.service.js');
const news = require('./news/news.service.js');

const comments = require('./comments/comments.service.js');
const followers = require('./followers/followers.service.js');
// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(users);
  app.configure(messages);
  app.configure(ideas);
  app.configure(votes);
  app.configure(news);
  app.configure(comments);
  app.configure(followers);
};
