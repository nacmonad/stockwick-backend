const path = require('path');
const fs = require('fs');
const https = require('https');

const favicon = require('serve-favicon');
const compress = require('compression');
const helmet = require('helmet');
const cors = require('cors');
const logger = require('./logger');

const feathers = require('@feathersjs/feathers');
const configuration = require('@feathersjs/configuration');
const express = require('@feathersjs/express');
const socketio = require('@feathersjs/socketio');


const middleware = require('./middleware');
const services = require('./services');
const appHooks = require('./app.hooks');
const channels = require('./channels');

const authentication = require('./authentication');

const mongodb = require('./mongodb');

const mongoose = require('./mongoose');

const app = express(feathers());

// Load app configuration
app.configure(configuration());
// Enable security, CORS, compression, favicon and body parsing
app.use(helmet());
app.use(cors());
app.use(compress());
app.use(express.json({limit: '512kb'}));
app.use(express.urlencoded({   limit: '512kb', extended: true }));

app.use(favicon(path.join(app.get('public'), 'favicon.ico')));
// Host the public folder
app.use('/', express.static(app.get('public'), { dotfiles:'allow'}));


// Set up Plugins and providers
app.configure(express.rest());
app.configure(socketio());

// Move over to Mongoose only
app.configure(mongodb);
app.configure(mongoose);

// Configure other middleware (see `middleware/index.js`)
app.configure(middleware);
app.configure(authentication);
// Set up our services (see `services/index.js`)
app.configure(services);
// Set up event channels (see channels.js)
app.configure(channels);

// Configure a middleware for 404s and the error handler
app.use(express.notFound());
app.use(express.errorHandler({ logger }));

app.hooks(appHooks);



//HTTPS

// const HTTPS_PORT = 3031;
// const privateKey = fs.readFileSync('/etc/letsencrypt/live/stage.stockwick.com/privkey.pem', 'utf8');
// const certificate = fs.readFileSync('/etc/letsencrypt/live/stage.stockwick.com/cert.pem', 'utf8');
// const ca = fs.readFileSync('/etc/letsencrypt/live/stage.stockwick.com/chain.pem', 'utf8');
//
// const credentials = {
// 	key: privateKey,
// 	cert: certificate,
// 	ca: ca
//
// };
//
// const httpsServer = https
// 	.createServer(credentials, app)
// 	.listen(HTTPS_PORT, () => {
// 			console.log(`HTTPS Server running on port ${HTTPS_PORT}`);
// 		});
//
// app.setup(httpsServer);

module.exports = app;
