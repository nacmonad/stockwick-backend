const assert = require('assert');
const app = require('../../src/app');

describe('\'mongoose-example\' service', () => {
  it('registered the service', () => {
    const service = app.service('mongoose-example');

    assert.ok(service, 'Registered the service');
  });
});
